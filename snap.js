function snapCrackle(maxValue) {
  let snap = '';
  for (let i = 1; i <= maxValue; i++) {
    if (snapCracklepPrime(i) && i % 2 === 1 && i % 5 === 0) {
      document.write('SnapCracklePrime, ');
      snap += 'SnapCracklePrime, ';
    } else if (i % 2 === 1 && i % 5 === 0) {
      document.write('SnapCrackle, ');
      snap += 'SnapCrackle, ';
    } else if (snapCracklepPrime(i) && i % 2 === 1) {
      document.write('SnapPrime, ');
      snap += 'SnapPrime, ';
    } else if (i === 2) {
      document.write('2 Prime, ');
      snap += '2 Prime, ';
    } else if (snapCracklepPrime(i)) {
      document.write('Prime, ');
      snap += 'Prime, ';
    } else if (i % 5 === 0) {
      document.write('Crackle, ');
      snap += 'Crackle, ';
    } else if (i % 2 === 1) {
      document.write('Snap, ');
      snap += 'Snap, ';
    } else {
      document.write(i + ', ');
      snap += i + ', ';
    }
  }
  console.log(snap);
}
function snapCracklepPrime(maxValue) {
  if (maxValue === 1) {
    return false;
  } else if (maxValue === 2) {
    return true;
  } else {
    for (let f = 2; f < maxValue; f++) {
      if (maxValue % f === 0) {
        return false;
      }
    }
    return true;
  }
}
